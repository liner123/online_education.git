package com.ithl.staservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author hl
 * @Data 2020/4/29
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.ithl"})
@MapperScan("com.ithl.staservice.mapper")
@EnableFeignClients
// 开启定时任务
@EnableScheduling
public class StaApplicationMain8007 {
    public static void main(String[] args) {
        SpringApplication.run(StaApplicationMain8007.class, args);
    }
}
