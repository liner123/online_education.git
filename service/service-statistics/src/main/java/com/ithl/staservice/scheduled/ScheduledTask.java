package com.ithl.staservice.scheduled;

import com.ithl.staservice.service.StatisticsDailyService;
import com.ithl.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 定时任务的类
 *
 * @author hl
 * @Data 2020/4/30
 */
@Component
public class ScheduledTask {
    @Autowired
    private StatisticsDailyService staService;

    // 每天网上24:00 点执行
    @Scheduled(cron = "0 0 0 * *  ?")
    public void Task() {
        String day = DateUtil.formatDate(DateUtil.addDays(new Date(), -1));
        staService.registerCount(day);
    }
}
