package com.ithl.eduorder.client;

import com.ithl.commonutils.orderVo.CourseDetailInfoOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hl
 * @Data 2020/4/27
 */
@Component
@FeignClient("service-edu")
public interface EduCourseClient {

    // 根据课程id查询课程信息
    @GetMapping("/eduservice/coursefront/getCourseInfoOrder")
    public CourseDetailInfoOrder getCourseInfoOrder(@RequestParam("courseId") String courseId);
}
