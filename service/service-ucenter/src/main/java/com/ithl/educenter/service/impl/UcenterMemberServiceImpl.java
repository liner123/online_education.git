package com.ithl.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ithl.commonutils.utils.JwtUtils;
import com.ithl.commonutils.utils.MD5;
import com.ithl.educenter.entity.UcenterMember;
import com.ithl.educenter.entity.vo.RegisterVo;
import com.ithl.educenter.mapper.UcenterMemberMapper;
import com.ithl.educenter.service.UcenterMemberService;
import com.ithl.serviceutils.exchandler.MyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author hl
 * @since 2020-04-23
 */
@Service
@Slf4j
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {
    @Autowired
    private RedisTemplate redisTemplate;

    // 登录
    @Override
    public String login(UcenterMember ucenterMember) {

        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();
        // 非空
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) {
            throw new MyException(1, "手机号或密码为空,登陆失败");
        }
        // 判断与数据库是否一样
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile", mobile);
        UcenterMember mMember = baseMapper.selectOne(queryWrapper);
        if (mMember == null) {
            throw new MyException(1, "账号错误或不存在");
        }
        // 判断密码 数据库的加了密所以需要先加密后比较 MD5
        String ectPassword = MD5.encrypt(password);
        if (!ectPassword.equals(mMember.getPassword())) {
            throw new MyException(1, "密码错误");
        }
        // 判断用户是否被禁用
        if (mMember.getIsDeleted()) {
            throw new MyException(1, "你的手机号已被禁用");
        }
        // 成功
        String token = JwtUtils.getJwtToken(mMember.getId(), mMember.getNickname());
        return token;
    }

    // 注册
    @Override
    public void register(RegisterVo registerVo) {
        Integer code = registerVo.getCode();
        String mobile = registerVo.getMobile();
        String nickname = registerVo.getNickname();
        String password = registerVo.getPassword();
        // 判断非空
        if (StringUtils.isEmpty(code) || StringUtils.isEmpty(mobile) || StringUtils.isEmpty(nickname) || StringUtils.isEmpty(password)) {
            throw new MyException(1, "信息不能为空,请检查");
        }
        // 判断手机号是否重复
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile", mobile);
        Integer count = baseMapper.selectCount(queryWrapper);
        if (count > 0) {
            throw new MyException(1, "手机号已存在");
        }
        Object redisCode = redisTemplate.opsForValue().get(mobile);
        // 判断验证码与存入redis的验证码是否一致
        if (!code.equals(redisCode)) {
            throw new MyException(1, "验证码错误,注册失败");
        }
        // 成功
        UcenterMember member = new UcenterMember();
        String emcPassword = MD5.encrypt(password);
        member.setNickname(nickname);
        member.setPassword(emcPassword);
        member.setMobile(mobile);
        member.setIsDisabled(false);
        member.setAvatar("https://tse1-mm.cn.bing.net/th/id/OIP.XFIkvTSgrujEEVCeDRVFdQHaHa?w=183&h=181&c=7&o=5&dpr=1.25&pid=1.7");
        int result = baseMapper.insert(member);
        if (result <= 0) {
            throw new MyException(1, "发生了错误,注册失败");
        }
    }

    // 根据openid 查询 微信对象
    @Override
    public UcenterMember getByOpenId(String openId) {
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid", openId);
        UcenterMember member = baseMapper.selectOne(queryWrapper);
        return member;
    }

    @Override
    public Integer getCountByDate(String day) {
        Integer count = baseMapper.getCountByDate(day);
        return count;
    }
}
