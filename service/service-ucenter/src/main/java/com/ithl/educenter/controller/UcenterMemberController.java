package com.ithl.educenter.controller;


import com.ithl.commonutils.utils.JwtUtils;
import com.ithl.commonutils.R;
import com.ithl.commonutils.commentVo.EduCommentVo;
import com.ithl.commonutils.orderVo.UcenterMemberOrder;
import com.ithl.educenter.entity.UcenterMember;
import com.ithl.educenter.entity.vo.RegisterVo;
import com.ithl.educenter.service.UcenterMemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author hl
 * @since 2020-04-23
 */
@RestController
@RequestMapping("/ucenter/member")
//@CrossOrigin
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService MemberService;

    // 登录
    @PostMapping("/login")
    public R loginUser(@RequestBody(required = false) UcenterMember ucenterMember) {
        String token = MemberService.login(ucenterMember);
        return R.ok().data("token", token);
    }

    // 注册
    @PostMapping("/register")
    public R register(@RequestBody RegisterVo registerVo) {
        MemberService.register(registerVo);
        return R.ok();
    }

    // 根据token获取用户信息
    @GetMapping("/getUserInfoByToken")
    public R getUserInfoByToken(HttpServletRequest request) {
        // 调jwt查id
        String uMemberId = JwtUtils.getMemberIdByJwtToken(request);
        // 根据id查用户
        UcenterMember uMember = MemberService.getById(uMemberId);
        return R.ok().data("userInfo", uMember);
    }

    // 根据用户id 查询用户信息
    @GetMapping("/getUserInfoOrder")
    public UcenterMemberOrder getUserInfoOrder(@RequestParam("memberId") String memberId) {
        UcenterMember ucenterMember = MemberService.getById(memberId);
        UcenterMemberOrder ucenterMemberOrder = new UcenterMemberOrder();
        if (ucenterMember != null) {
            BeanUtils.copyProperties(ucenterMember, ucenterMemberOrder);
        }
        return ucenterMemberOrder;
    }

    // 根据用户id 查询用户信息 返回给评论用
    @GetMapping("/getUserInfoComment")
    public EduCommentVo getUserInfoComment(@RequestParam("memberId") String memberId) {
        UcenterMember ucenterMember = MemberService.getById(memberId);
        EduCommentVo eduCommentVo = new EduCommentVo();
        if (ucenterMember != null) {
            BeanUtils.copyProperties(ucenterMember, eduCommentVo);
        }
        return eduCommentVo;
    }

    // 查询当天的注册人数
    @GetMapping("/getRegisterCount")
    public int getRegisterCount(@RequestParam("day") String day) {
        Integer count = MemberService.getCountByDate(day);
        return count;
    }
}

