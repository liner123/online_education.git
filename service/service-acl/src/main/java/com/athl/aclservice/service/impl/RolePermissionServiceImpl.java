package com.athl.aclservice.service.impl;


import com.athl.aclservice.entity.RolePermission;
import com.athl.aclservice.mapper.RolePermissionMapper;
import com.athl.aclservice.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author hl
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
