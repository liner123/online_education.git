package com.athl.aclservice.service;


import com.athl.aclservice.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务类
 *
 * @author hl
 */
public interface UserRoleService extends IService<UserRole> {

}
