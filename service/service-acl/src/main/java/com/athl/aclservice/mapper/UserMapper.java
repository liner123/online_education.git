package com.athl.aclservice.mapper;


import com.athl.aclservice.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户表 Mapper 接口
 *
 * @author hl
 */
public interface UserMapper extends BaseMapper<User> {

}
