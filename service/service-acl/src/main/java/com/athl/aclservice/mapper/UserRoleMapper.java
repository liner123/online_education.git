package com.athl.aclservice.mapper;


import com.athl.aclservice.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Mapper 接口
 *
 * @author hl
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
