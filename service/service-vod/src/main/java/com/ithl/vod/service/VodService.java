package com.ithl.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author hl
 */
public interface VodService {
    // 上传视频
    String uploadAliyVideo(MultipartFile file);

    void removeMoreAliyVideo(List<String> videoIdList);

}
