package com.ithl.edumsm.service;

import java.util.Map;

/**
 * @author hl
 * @Data 2020/4/22
 */
public interface MsmService {
    boolean sendMessage(Map<String, Object> param, String phone);
}
