package com.ithl.educms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author hl
 * @Data 2020/4/21
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.ithl"})
@MapperScan("com.ithl.educms.mapper")
public class CmsApplicationMain8004 {
    public static void main(String[] args) {
        SpringApplication.run(CmsApplicationMain8004.class, args);
    }
}
