package com.ithl.educms.controller;

import com.ithl.commonutils.R;
import com.ithl.educms.entity.CrmBanner;
import com.ithl.educms.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hl
 * @Data 2020/4/21
 * <p>
 * 这个是前台用户玩的
 */
@RestController
//@CrossOrigin
@RequestMapping("/educms/bannerfront")
public class BannerFrontController {
    @Autowired
    private CrmBannerService crmBannerService;

    // 查询前两条banner
    @GetMapping("getAllBanner")
    public R getAllBanner() {
        List<CrmBanner> crmBannerList = crmBannerService.selectAllBanner();
        return R.ok().data("list", crmBannerList);
    }
}
