package com.ithl.eduservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hl
 * @Data 2020/4/29
 */
@FeignClient("service-order")
@Component
public interface OrderClient {
    // 根据课程id和用户id查询订单状态
    @GetMapping("/eduorder/order/isBuyCourse")
    public boolean isBuyCourse(@RequestParam("courseId") String courseId, @RequestParam("memberId") String memberId);
}
