package com.ithl.eduservice.controller.front;

import com.ithl.commonutils.R;
import com.ithl.eduservice.entity.EduCourse;
import com.ithl.eduservice.entity.EduTeacher;
import com.ithl.eduservice.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hl
 * @Data 2020/4/21
 */
@RestController
@RequestMapping("/eduservice/indexfront")
//@CrossOrigin
public class IndexFrontController {
    @Autowired
    private EduCourseService courseService;

    // 查询前八条热门课程，前四名名师
    @GetMapping("/index")
    public R index() {
        // 查询前八条热门课程
        List<EduCourse> eduCourseList = courseService.cacheCourseList();
        // 查询前四名师
        List<EduTeacher> eduTeacherList = courseService.cacheTeacherList();
        return R.ok().data("eduList", eduCourseList).data("teacherList", eduTeacherList);
    }
}
