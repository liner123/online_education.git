package com.ithl.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ithl.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author hl
 * @since 2020-04-13
 */
public interface EduTeacherService extends IService<EduTeacher> {

    Map<String, Object> getTeacherFront(Page<EduTeacher> teacherPage);
}
