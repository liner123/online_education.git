package com.ithl.eduservice.entity.chapter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author hl
 * <p>
 * 小节
 */
@Data
public class VideoVo implements Serializable {

    private static final long serialVersionUID = -6533806824764816651L;
    private String id;
    @ApiModelProperty(value = "课程标题")
    private String title;
    @ApiModelProperty(value = "课程排序")
    private Integer sort;
    @ApiModelProperty(value = "课程id")
    private String videoSourceId;
}
