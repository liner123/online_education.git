package com.ithl.eduservice.client;

import com.ithl.commonutils.R;
import com.ithl.eduservice.client.impl.VodClientFeignImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author hl
 */
// 指定发生错误跳转实现类
@FeignClient(name = "service-vod", fallback = VodClientFeignImpl.class)
@Component
public interface VodClient {

    // 定义调用的方法路径
    // 根据id删除视频
    @DeleteMapping("/eduvod/video/deleteAliyVideo/{videoId}")
    public R deleteAliyVideo(@PathVariable("videoId") String videoId);

    // 删除多个视频
    @DeleteMapping("/eduvod/video/deleteBatch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList);
}
