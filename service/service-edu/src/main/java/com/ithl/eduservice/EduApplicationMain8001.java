package com.ithl.eduservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hl
 */
@SpringBootApplication
@MapperScan(basePackages = "com.ithl.eduservice.mapper")
// 服务注册
@EnableDiscoveryClient
// 服务调用
@EnableFeignClients
public class EduApplicationMain8001 {
    public static void main(String[] args) {
        SpringApplication.run(EduApplicationMain8001.class, args);
    }
}
