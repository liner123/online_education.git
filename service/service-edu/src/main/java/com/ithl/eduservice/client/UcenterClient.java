package com.ithl.eduservice.client;

import com.ithl.commonutils.commentVo.EduCommentVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hl
 * @Data 2020/4/28
 */
@Component
@FeignClient("service-ucenter")
public interface UcenterClient {

    // 根据用户id 查询用户信息 返回给评论用
    @GetMapping("/ucenter/member/getUserInfoComment")
    public EduCommentVo getUserInfoComment(@RequestParam("memberId") String memberId);
}
