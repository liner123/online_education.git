package com.ithl.eduservice.client.impl;

import com.ithl.commonutils.R;
import com.ithl.eduservice.client.VodClient;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 出现错误后执行方法
 *
 * @author hl
 */
@Component
public class VodClientFeignImpl implements VodClient {
    @Override
    public R deleteAliyVideo(String videoId) {
        return R.error().message("删除视频出错了");
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        return R.error().message("删除多个视频出错了");
    }
}
